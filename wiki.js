/*
     Copyright 2020 SJULTRA, inc.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
const fs = require('fs');
const fse = require('fs-extra');
const uuid = require("uuid");

const { Csv2md } = require('csv2md');

const getFileList = (path) => {
    //Parse each folder until only files are found
    images = []
    csvs = []
    filenames = fs.readdirSync(path)
    for (file of filenames) {
        if (!file.startsWith(".")) {
            newpath = path + "/" + file
            if (fs.lstatSync(newpath).isDirectory()) {
                getFileList(newpath)
            } else {
                if (newpath.includes(".png")) {
                    images.push(newpath)
                }

                if (newpath.includes(".csv")) {
                    csvs.push(newpath)
                }
            }
        }
    }
    return { images, csvs }
}

const Wiki = async (config) => {
    console.log(config)

    if (config.wiki && config.wiki.length >= 1) {
        for (const entry of config.wiki) {
            files = getFileList(entry.source)
            filesPath = getFileList(`${entry.path}/${entry.images}`)
            fileContent=""

            for (const entryimg of filesPath.images) {
                fname=entryimg.split('/').pop()
                fileContent+=`![${fname}](./${entry.images}/${fname})\n`
            }

            for (const entrycsv of files.csvs) {
                const csvString = fs.readFileSync(entrycsv).toString()
                const csv2md = new Csv2md({ csvDelimiter:'\t' })
                fmtcsv=csvString.replace("\"sep=\t\"\n","")
                const markdown =  await csv2md.convert(fmtcsv)    
                entrycsv.split('/').pop()
                fname=entrycsv.split('/').pop().replace(".csv","")
                fileContent+=`${fname}\n`
                fileContent+=`${markdown}\n`
            }

            fs.writeFileSync(
                `${entry.path}/panpc_report.md`
                ,fileContent,
                function (err) {
                  if (err) return console.log(err)
                  console.log(`Wrote succesfully the Readme.md file`)
                }
            )
        }
    }
}

module.exports = { Wiki }
