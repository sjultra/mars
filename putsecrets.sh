#/bin/bash

set -xv
input=$1
while IFS= read -r line
do
    if [[ "${line}" == *"\${{"* ]]; then
        var_name="$(echo "$line" | sed 's/.*${{\(.*\)}}.*/\1/')"
        eval "var_value=\${$var_name}"
        echo "$var_value"
        sed -ie '0,/${{.*}}/s##'"$var_value"'#' "$input"
    fi
done < "$input"
