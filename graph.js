const Quiche = require('quiche'),
    axios = require('axios'),
    fs = require('fs')

const flattenObject = (obj) => {
    const flattened = {}

    Object.keys(obj).forEach((key) => {
        if (typeof obj[key] === 'object' && obj[key] !== null) {
            Object.assign(flattened, flattenObject(obj[key]))
        } else {
            flattened[key] = obj[key]
        }
    })

    return flattened
}

const dataformatter = (data) => {
    const newDates = []
    const newValues = []
    for (const entry of data.dataPoints) {
        newDates.push(new Date(entry.timeRange.startTime).toISOString().split('T')[0])
        newValues.push(Object.values(flattenObject(entry.counts)).reduce((a, b) => a + b, 0))
    }
    return { newDates, newValues, purchases: data.workloadsPurchased, avg: newValues.reduce((a, b) => a + b, 0) / newValues.length }
}

const composePos = (length, pos, data) => {
    const arr = new Array(length).fill(0)
    arr[pos] = data
    return arr
}

const graphGenSubscription = (entries) => {
    var chart = new Quiche('line');
    chart.setWidth(800);
    chart.setHeight(300);

    chart.setTitle('Cloud Usage graph');
    chart.addData([entries.purchases, entries.purchases], `purchased licenses: ${entries.purchases}`, '008000');
    chart.addData([entries.avg, entries.avg], `average license usage: ${entries.avg}`, '0000FF');
    chart.addData(entries.newValues, `subscription usage: min: ${Math.min(...entries.newValues)} - max: ${Math.max(...entries.newValues)}`, 'FF4500');
    chart.addAxisLabels('x', entries.newDates);
    chart.setAutoScaling();
    chart.setAxisRange('y', 0, 150, 10);
    chart.setTransparentBackground();
    return imageUrl = chart.getUrl(true); // First param controls http vs. https
}

const download = (url, image_path) =>
    axios({
        url,
        responseType: 'stream',
    }).then(
        response =>
            new Promise((resolve, reject) => {
                response.data
                    .pipe(fs.createWriteStream(image_path))
                    .on('finish', () => resolve())
                    .on('error', e => reject(e));
            }),
    ).catch(error=>{
        console.log(error)
    });


const generateSubscriptionChart = async (data, name) => {
    const entries = dataformatter(data)
    await download(graphGenSubscription(entries), `${name}.png`);

}
const interndataformatter = (data) => {
    for (const entry of data)
        entry.timestamp = new Date(entry.timestamp).toISOString().split('T')[0]
    return data
}
const graphGenInventoryTrend = (entries) => {
    var chart = new Quiche('line');

    chart.setWidth(800);
    chart.setHeight(300);

    chart.setTitle('Cloud Usage Trend graph');

    chart.addData([...entries.map(({ totalResources }) => totalResources)], `totalResources: min: ${Math.min(...entries.map(({ totalResources }) => totalResources))} - max: ${Math.max(...entries.map(({ totalResources }) => totalResources))}`, '008000');
    chart.addData([...entries.map(({ failedResources }) => failedResources)], `Fail: min: ${Math.min(...entries.map(({ failedResources }) => failedResources))} - max: ${Math.max(...entries.map(({ failedResources }) => failedResources))}`, 'FF0000');
    chart.addData([...entries.map(({ passedResources }) => passedResources)], `Pass: min: ${Math.min(...entries.map(({ passedResources }) => passedResources))} - max: ${Math.max(...entries.map(({ passedResources }) => passedResources))}`, '00FF00');


    chart.addAxisLabels('x', entries.map(({ timestamp }) => timestamp));
    chart.setAutoScaling();
    chart.setAxisRange('y', 0, 150, 10);
    chart.setTransparentBackground();
    return imageUrl = chart.getUrl(true); // First param controls http vs. https
}

const generateInventoryTrendChart = async (data, name) => {
    const entries = interndataformatter(data)
    await download(graphGenInventoryTrend(entries), `${name}.png`);

}


const generateGraphAssets = (data) => {
    images = []
    dataS = data.summary

    var chart = new Quiche('bar');

    chart.setWidth(800);
    chart.setHeight(350);

    chart.setTitle('Asset Summary');
    chart.setBarStacked(); // Stacked chart
    chart.setBarWidth(0);
    chart.setBarSpacing(5); // 5 pixles between bars/groups
    chart.setLegendTop(); // Put legend at top
    chart.setTransparentBackground(); // Make background transparent
    chart.setBarHorizontal();

    chart.addData([dataS.totalResources], `Resources: ${dataS.totalResources}`, '1CFFFF');
    chart.addData([0, dataS.failedResources], `Fail: ${dataS.failedResources}`, 'FF0000');
    chart.addData([0, dataS.passedResources], `Pass: ${dataS.passedResources}`, '00FF00');
    chart.addData([0, 0, dataS.lowSeverityFailedResources], `Low: ${dataS.lowSeverityFailedResources}`, '008000');
    chart.addData([0, 0, dataS.mediumSeverityFailedResources], `Medium: ${dataS.mediumSeverityFailedResources}`, 'EFF542');
    chart.addData([0, 0, dataS.highSeverityFailedResources], `High: ${dataS.highSeverityFailedResources}`, 'FF681C');

    chart.setAutoScaling(); // Auto scale y axis
    chart.setAxisRange('x', 0, 3000, 300);

    imageUrl = chart.getUrl(true); // First param controls http vs. https
    images.push(imageUrl)
    dataAg = data.groupedAggregates


    var chart = new Quiche('bar');
    chart.setWidth(800);
    chart.setHeight(350);

    chart.setTitle('Resource Summary');
    chart.setBarStacked(); // Stacked chart
    chart.setBarWidth(10);
    chart.setBarSpacing(5); // 5 pixles between bars/groups
    chart.setLegendTop(); // Put legend at top
    chart.setTransparentBackground(); // Make background transparent
    console.log(dataAg)
    for (const entry in dataAg) {
        chart.addData(composePos(dataAg.length, entry, dataAg[entry].failedResources), `${dataAg[entry].cloudTypeName} Fail: ${dataAg[entry].failedResources}`, 'FF0000');
        chart.addData(composePos(dataAg.length, entry, dataAg[entry].passedResources), `${dataAg[entry].cloudTypeName} Pass: ${dataAg[entry].passedResources}`, '00FF00');
    }
    chart.setAutoScaling(); // Auto scale y axis
    chart.setAxisRange('x', 0, 3000, 300);

    var imageUrl = chart.getUrl(true); // First param controls http vs. https
    images.push(imageUrl)

    return images
}



const generateAssetsCharts = async (data, name) => {
    images = generateGraphAssets(data)
    for (const no in images)
        await download(images[no], `${name}_${no}.png`);
}

module.exports = { generateSubscriptionChart, generateAssetsCharts, generateInventoryTrendChart }
